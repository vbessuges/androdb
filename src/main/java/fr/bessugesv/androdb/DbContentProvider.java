package fr.bessugesv.androdb;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import fr.bessugesv.androdb.fields.UriTableMatcher;

/**
 * Created by Vincent Bessuges on 30/11/2014.
 */
public abstract class DbContentProvider extends ContentProvider {
    protected Database mDb;
    protected UriTableMatcher mUriTableMatcher;




    @Override
    public boolean onCreate() {
        mDb = initDatabase(getContext());

        DBSchema contract = mDb.getContract();

        mUriTableMatcher = new UriTableMatcher(getAuthority(), contract);

        return true;
    }

    @Override
    // TODO projection is not used
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor c = mUriTableMatcher.matchTable(uri).query(selection, selectionArgs, sortOrder);
        if(c != null) c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public String getType(Uri uri) {
        return mUriTableMatcher.matchType(uri);
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long inserted = mDb.getWritableDatabase().insert(mUriTableMatcher.matchTable(uri).getSchema().getTableName(), null, values);
        getContext().getContentResolver().notifyChange(uri, null);
        return ADContentUris.withAppendedId(uri, inserted);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if(ADContentUris.getTypeNumber(mUriTableMatcher.matchType(uri)) == ADContentUris.SINGULAR){
            long id = ADContentUris.parseId(uri);
            selection = BaseColumns._ID + " = ?";
            selectionArgs = new String[] {String.valueOf(id)};
        }
        // the contract of the ContentProvider.delete method indicates that it should always return the number of affected rows.
        // passing "null" as a selection parameter will delete all rows, but then the DB delete will return 0.
        // to force this call to count the deleted rows we have to pass "1" as a selection parameter
        if(selection == null){
            selection = "1";
        }
        int deletedCount =  mDb.getWritableDatabase().delete(mUriTableMatcher.matchTable(uri).getSchema().getTableName(), selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return deletedCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int changedRows = mDb.getWritableDatabase().update(mUriTableMatcher.matchTable(uri).getSchema().getTableName(), values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return changedRows;
    }

    protected abstract String getAuthority();
    protected abstract Database initDatabase(Context context);


}
