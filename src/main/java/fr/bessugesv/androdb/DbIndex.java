package fr.bessugesv.androdb;

import android.util.Log;

import java.util.EnumSet;

import fr.bessugesv.androdb.fields.AbsDbField;

/**
 * Created by Vincent on 19/11/2014.
 */
public class DbIndex {
    public static final String LOG = DbIndex.class.getSimpleName();

    private String mIndexName;
    private String mColumnName;

    private boolean mIsUnique;

    public DbIndex(String indexName, String columnName){
        mIndexName = indexName;
        mColumnName = columnName;
    }

    public DbIndex(String indexName, String columnName, EnumSet<AbsDbField.Constraint> constraints){
        this(indexName, columnName);
        for(AbsDbField.Constraint s : constraints){
            switch(s){
                case UNIQUE: mIsUnique = true;
                    break;
                default:
                    Log.e(LOG, "Constraint " + s + " for DbIndex not implemented.");
                    break;
            }
        }
    }

    public String toDBCreateString(String tableName){
        return "CREATE "+(mIsUnique ? "UNIQUE " : "")+"INDEX "+mIndexName+" ON "+tableName+" ("+mColumnName+")";
    }

}
