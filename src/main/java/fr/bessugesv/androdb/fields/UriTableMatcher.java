package fr.bessugesv.androdb.fields;

import android.content.UriMatcher;
import android.net.Uri;

import java.util.ArrayList;

import fr.bessugesv.androdb.ADContentUris;
import fr.bessugesv.androdb.DBSchema;
import fr.bessugesv.androdb.DbTable;

/**
 * Created by Vincent Bessuges on 30/11/2014.
 */
public class UriTableMatcher extends UriMatcher {
    private DbTable [] matches;
    private String [] types;
    public UriTableMatcher(String authority, DBSchema dbSchema) {
        super(UriMatcher.NO_MATCH);

        ArrayList<DbTable> tables = dbSchema.getTables();
        int count = tables.size();
        matches = new DbTable[count];
        types = new String[count * 2];
        int matchIndex = 0;
        for(int i = 0 ; i < count ; i++){
            DbTable table = tables.get(i);
            String tableName = table.getSchema().getTableName();
            matches[i] = table;

            addURI(authority,  tableName, matchIndex);
            types[matchIndex] = ADContentUris.buildType(ADContentUris.PLURAL, authority, tableName);
            matchIndex++;

            addURI(authority,  tableName+"/#", matchIndex);
            types[matchIndex] = ADContentUris.buildType(ADContentUris.SINGULAR, authority, tableName);
            matchIndex++;
        }
    }

    public DbTable matchTable(Uri uri) {
        return matches[match(uri) / 2];
    }

    public String matchType(Uri uri){
        return types[match(uri)];
    }
}
