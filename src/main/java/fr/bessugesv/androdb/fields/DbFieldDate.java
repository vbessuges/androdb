package fr.bessugesv.androdb.fields;

import android.content.ContentValues;

import java.util.Date;
import java.util.EnumSet;

/**
 * Created by Vincent on 22/11/2014.
 */
public class DbFieldDate extends AbsDbFieldInteger<Date> {

    public DbFieldDate(String name) {
        super(name);
    }

    public DbFieldDate(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    public Integer getDefaultValue() {
        return (int)(new Date().getTime() / 1000);
    }

    @Override
    protected Integer valueToDbValue(Date value) {
        return (int)(value.getTime() / 1000);
    }

    @Override
    protected Date dbValueToValue(Integer value) {
        return new Date(value * 1000L);
    }

    @Override
    public void _fillRow(DbRow row, Date value) {
        row.putDate(mName, value);
    }

    @Override
    public void _fillValues(ContentValues values, Date value) {
        values.put(mName, valueToDbValue(value));
    }
}
