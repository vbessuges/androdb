package fr.bessugesv.androdb.fields;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.EnumSet;

/**
 * Created by Vincent Bessuges on 12/04/2015.
 */
public abstract class AbsDbFieldInteger<T> extends AbsDbFieldAbsInteger<T, Integer> {

    public AbsDbFieldInteger(String name) {
        super(name);
    }

    public AbsDbFieldInteger(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    protected void typedBind(SQLiteStatement stmt, Integer dbValue, int index) {
        stmt.bindLong(index, dbValue);
    }

    @Override
    protected Integer getDefaultValue() {
        return 0;
    }

    @Override
    public Integer extractValue(Cursor cursor, int columnIndex) {
        return cursor.getInt(columnIndex);
    }
}
