package fr.bessugesv.androdb.fields;

import android.content.ContentValues;

import java.util.EnumSet;

/**
 * Created by Vincent on 18/11/2014.
 */
public class DbFieldBoolean extends AbsDbFieldInteger<Boolean> {

    public DbFieldBoolean(String name) {
        super(name);
    }

    public DbFieldBoolean(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    protected Integer valueToDbValue(Boolean value) {
        return value ? 1 : 0;
    }

    @Override
    protected Boolean dbValueToValue(Integer value) {
        return value != null && value == 1;
    }

    @Override
    public void _fillRow(DbRow row, Boolean value) {
        row.putBoolean(mName, value);
    }

    @Override
    public void _fillValues(ContentValues values, Boolean value) {
        values.put(mName, value);
    }
}
