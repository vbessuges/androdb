package fr.bessugesv.androdb.fields;

import android.content.ContentValues;
import android.text.TextUtils;

import java.util.EnumSet;

/**
 * Created by Vincent Bessuges on 12/04/2015.
 */
public class DbFieldString extends AbsDbFieldText<String> {

    public DbFieldString(String name) {
        super(name);
    }

    public DbFieldString(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    protected boolean isValueNull(String value) {
        return TextUtils.isEmpty(value);
    }

    @Override
    protected String valueToDbValue(String value) {
        return value;
    }

    @Override
    protected String dbValueToValue(String value) {
        return value;
    }

    @Override
    public void _fillRow(DbRow row, String value) {
        row.putString(mName, value);
    }

    @Override
    public void _fillValues(ContentValues values, String value) {
        values.put(mName, value);
    }
}
