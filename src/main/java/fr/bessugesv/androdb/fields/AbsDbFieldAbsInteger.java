package fr.bessugesv.androdb.fields;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.EnumSet;

/**
 * Created by Vincent on 19/11/2014.
 */
public abstract class AbsDbFieldAbsInteger<T, DBT> extends AbsDbField<T, DBT> {
    public AbsDbFieldAbsInteger(String name) {
        super(name);
    }

    public AbsDbFieldAbsInteger(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    public String getType() {
        return "INTEGER";
    }
}
