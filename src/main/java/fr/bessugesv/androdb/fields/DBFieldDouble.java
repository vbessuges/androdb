package fr.bessugesv.androdb.fields;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.EnumSet;

/**
 * Created by Vincent on 19/11/2014.
 */
public class DBFieldDouble extends AbsDbFieldReal<Double> {

    public DBFieldDouble(String name) {
        super(name);
    }

    public DBFieldDouble(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    protected Double valueToDbValue(Double value) {
        return value;
    }

    @Override
    protected Double dbValueToValue(Double value) {
        return value;
    }

    @Override
    public void _fillRow(DbRow row, Double value) {
        row.putDouble(mName, value);
    }

    @Override
    public void _fillValues(ContentValues values, Double value) {
        values.put(mName, value);
    }
}
