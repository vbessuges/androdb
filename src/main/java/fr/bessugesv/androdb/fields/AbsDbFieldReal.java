package fr.bessugesv.androdb.fields;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.EnumSet;

/**
 * Created by Vincent Bessuges on 12/04/2015.
 */
public abstract class AbsDbFieldReal<T> extends AbsDbField<T, Double> {

    public AbsDbFieldReal(String name) {
        super(name);
    }

    public AbsDbFieldReal(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    public String getType() {
        return "DOUBLE";
    }

    @Override
    protected void typedBind(SQLiteStatement stmt, Double value, int index) {
        stmt.bindDouble(index, value);
    }

    @Override
    protected Double getDefaultValue() {
        return 0.d;
    }

    @Override
    public Double extractValue(Cursor cursor, int columnIndex) {
        return cursor.getDouble(columnIndex);
    }
}
