package fr.bessugesv.androdb.fields;

import android.provider.BaseColumns;

import java.util.EnumSet;

/**
 * Created by Vincent on 6/25/2016.
 */
public class DbIdField extends DbFieldLong {
    public DbIdField() {
        super(BaseColumns._ID, EnumSet.of(AbsDbField.Constraint.PRIMARY_KEY));
    }
}
