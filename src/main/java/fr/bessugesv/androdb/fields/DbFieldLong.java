package fr.bessugesv.androdb.fields;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.EnumSet;

/**
 * Created by Vincent Bessuges on 06/04/2015.
 */
public class DbFieldLong extends AbsDbFieldAbsInteger<Long, Long> {

    public DbFieldLong(String name) {
        super(name);
    }

    public DbFieldLong(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    protected void typedBind(SQLiteStatement stmt, Long dbValue, int index) {
        stmt.bindLong(index, dbValue);
    }

    @Override
    protected Long valueToDbValue(Long value) {
        return value;
    }

    @Override
    protected Long getDefaultValue() {
        return 0L;
    }

    @Override
    protected Long dbValueToValue(Long value) {
        return value;
    }

    @Override
    public Long extractValue(Cursor cursor, int columnIndex) {
        return cursor.getLong(columnIndex);
    }


    @Override
    public void _fillRow(DbRow row, Long value) {
        row.putLong(mName, value);
    }

    @Override
    public void _fillValues(ContentValues values, Long value) {
        values.put(mName, value);
    }
}
