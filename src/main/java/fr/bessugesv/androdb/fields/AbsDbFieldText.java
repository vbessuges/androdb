package fr.bessugesv.androdb.fields;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.EnumSet;

/**
 * Created by Vincent Bessuges on 19/11/2014.
 */
public abstract class AbsDbFieldText<T> extends AbsDbField<T, String> {

    public AbsDbFieldText(String name) {
        super(name);
    }

    public AbsDbFieldText(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    public String extractValue(Cursor cursor, int columnIndex) {
        return cursor.getString(columnIndex);
    }

    @Override
    protected void typedBind(SQLiteStatement stmt, String value, int index) {
        stmt.bindString(index, value);
    }

    @Override
    public String getType() {
        return "TEXT";
    }

    @Override
    public String getDefaultValue() {
        return "\"\"";
    }

}
