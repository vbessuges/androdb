package fr.bessugesv.androdb.fields;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by Vincent on 18/11/2014.
 */
public class DbRow {
    private HashMap<String, Double> mMapDoubles;
    private HashMap<String, String> mMapStrings;
    private HashMap<String, Integer> mMapIntegers;
    private HashMap<String, Long> mMapLongs;
    private HashMap<String, Boolean> mMapBooleans;
    private HashMap<String, Date> mMapDates;


    public void putDouble(String key, Double value){
        if(mMapDoubles == null) mMapDoubles = new HashMap<String, Double>();
        mMapDoubles.put(key, value);
    }
    public void putString(String key, String value){
        if(mMapStrings == null) mMapStrings = new HashMap<String, String>();
        mMapStrings.put(key, value);
    }
    public void putInteger(String key, Integer value){
        if(mMapIntegers == null) mMapIntegers = new HashMap<String, Integer>();
        mMapIntegers.put(key, value);
    }
    public void putLong(String key, Long value){
        if(mMapLongs == null) mMapLongs = new HashMap<String, Long>();
        mMapLongs.put(key, value);
    }
    public void putBoolean(String key, Boolean value){
        if(mMapBooleans == null) mMapBooleans = new HashMap<String, Boolean>();
        mMapBooleans.put(key, value);
    }
    public void putDate(String key, Date date){
        if(mMapDates == null) mMapDates = new HashMap<String, Date>();
        mMapDates.put(key, date);
    }

    public Double getDouble(String key){
        return getDouble(key, 0d);
    }
    public Double getDouble(String key, Double valueIfNull){
        Double res;
        if(mMapDoubles == null || (res = mMapDoubles.get(key)) == null) return valueIfNull;
        return res;
    }
    public String getString(String key){
        return getString(key, null);
    }
    public String getString(String key, String valueIfNull){
        String res;
        if(mMapStrings == null || (res = mMapStrings.get(key)) == null) return valueIfNull;
        return res;
    }
    public Integer getInteger(String key){
        return getInteger(key, 0);
    }
    public Integer getInteger(String key, Integer valueIfNull){
        Integer res;
        if(mMapIntegers == null || (res = mMapIntegers.get(key)) == null) return valueIfNull;
        return res;
    }
    public Long getLong(String key){
        return getLong(key, 0L);
    }
    public Long getLong(String key, Long valueIfNull){
        Long res;
        if(mMapLongs == null || (res = mMapLongs.get(key)) == null) return valueIfNull;
        return res;
    }
    public Boolean getBoolean(String key){
        return getBoolean(key, false);
    }
    public Boolean getBoolean(String key, Boolean valueIfNull){
        Boolean res;
        if(mMapBooleans == null || (res = mMapBooleans.get(key)) == null) return valueIfNull;
        return res;
    }


    public Date getDate(String key) {
        return getDate(key, null);
    }
    public Date getDate(String key, Date valueIfNull) {
        Date res;
        if(mMapDates == null || (res = mMapDates.get(key)) == null) return valueIfNull;
        return res;
    }
}
