package fr.bessugesv.androdb.fields;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.EnumSet;
import java.util.HashMap;

/**
 * Created by Vincent on 18/11/2014.
 */
public abstract class AbsDbField<T, DBT> {
    public static final String LOG = AbsDbField.class.getSimpleName();

    public enum Constraint {
        NOT_NULL, PRIMARY_KEY, UNIQUE
    }


    protected String mName;
    protected boolean mIsPrimaryKey = false;
    protected boolean mIsNotNull = false;

    public void setPrimaryKey(boolean isPrimaryKey){
        mIsPrimaryKey = isPrimaryKey;
    }

    public void setNotNull(boolean isNotNull){
        mIsNotNull = isNotNull;
    }


    public AbsDbField(String name){
        mName = name;
    }

    public AbsDbField(String name, EnumSet<Constraint> constraints){
        mName = name;
        for(Constraint s : constraints){
            switch(s){
                case NOT_NULL: 		mIsNotNull = true; 		break;
                case PRIMARY_KEY: 	mIsPrimaryKey = true; 	break;
                case UNIQUE: 		Log.e(LOG, "Constraint UNIQUE for DbField not yet implemented!"); 	break;
            }
        }
    }

    public String getName() {
        return mName;
    }

    @SuppressWarnings("unchecked")
    public void bind(SQLiteStatement stmt, T value, int index) {
        if(value == null){
            Log.d(LOG, "Binding "+mName+" to null");
            stmt.bindNull(index);
        }
        else {
            try {
                Log.d(LOG, "Binding "+mName+" of type "+getType()+" to "+value);
                typedBind(stmt, valueToDbValue(value), index);
            }
            catch(ClassCastException cce){
                String msg = "Error while binding field "+mName+": Could not parse value("+value+" of type "
                        +value.getClass()+") into type "+getType();
                throw new IllegalArgumentException(msg, cce);
            }
        }

    }


    public void fillRow(Cursor cursor, DbRow row) {
        _fillRow(row, dbValueToValue(extractValue(cursor, cursor.getColumnIndex(mName))));
    }

    public void fillValues(ContentValues values, T value) {
        if(!mIsNotNull && isValueNull(value)){
            values.putNull(mName);
        }
        else {
            _fillValues(values, value);
        }
    }
    protected boolean isValueNull(T value){
        return value == null;
    }

    public DBT extractDbValueFromLine(HashMap<String, Object> line) {
        return valueToDbValue((T) line.get(mName));
    }

    public final String getDefault(){
        return String.valueOf(getDefaultValue());
    }

    public String toDBCreateString() {
        String res = mName + " " + getType();
        if(mIsPrimaryKey) res += " PRIMARY KEY";
        if(mIsNotNull) res += " NOT NULL DEFAULT " + getDefault();
        return res;
    }

    public abstract String getType();
    protected abstract void typedBind(SQLiteStatement stmt, DBT dbValue, int index);
    protected abstract DBT valueToDbValue(T value);
    protected abstract DBT getDefaultValue();
    protected abstract T dbValueToValue(DBT value);
    public abstract DBT extractValue(Cursor cursor, int columnIndex);
    // TODO should either give mName as param, or maybe the abstract method could just be something like getValueToFillRow
    public abstract void _fillRow(DbRow row, T value);
    public abstract void _fillValues(ContentValues values, T value);

}
