package fr.bessugesv.androdb.fields;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.util.EnumSet;

/**
 * Created by Vincent Bessuges on 12/04/2015.
 */
public class DbFieldInteger extends AbsDbFieldInteger<Integer> {

    public DbFieldInteger(String name) {
        super(name);
    }

    public DbFieldInteger(String name, EnumSet<Constraint> constraints) {
        super(name, constraints);
    }

    @Override
    protected void typedBind(SQLiteStatement stmt, Integer dbValue, int index) {
        stmt.bindLong(index, dbValue);
    }

    @Override
    protected Integer valueToDbValue(Integer value) {
        return value;
    }

    @Override
    protected Integer getDefaultValue() {
        return 0;
    }

    @Override
    protected Integer dbValueToValue(Integer value) {
        return value;
    }

    @Override
    public Integer extractValue(Cursor cursor, int columnIndex) {
        return cursor.getInt(columnIndex);
    }

    @Override
    public void _fillRow(DbRow row, Integer value) {
        row.putInteger(mName, value);
    }

    @Override
    public void _fillValues(ContentValues values, Integer value) {
        values.put(mName, value);
    }
}
