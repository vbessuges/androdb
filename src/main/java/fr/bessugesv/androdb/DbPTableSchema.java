package fr.bessugesv.androdb;

import android.content.ContentValues;
import android.provider.BaseColumns;
import android.text.TextUtils;

import java.util.HashMap;

import fr.bessugesv.androdb.fields.AbsDbField;
import fr.bessugesv.androdb.fields.DbRow;

/**
 * Created by VBessuges on 10/04/2015.
 */
public abstract class DbPTableSchema<T> extends DbTableSchema {

    public ContentValues contentValuesFrom(T instance){
        ContentValues values = new ContentValues();
        HashMap<String, Object> line = lineFromInstance(instance);
        for(AbsDbField field : mFields){
            // we don't want to add the id in the content values
            // as this method is called for updates (the id must not be updated)
            // or insertions (the id will be set by the DB)
            if(field.getName().equals(BaseColumns._ID)) continue;
            field.fillValues(values, line.get(field.getName()));
        }
        return values;
    }

    protected abstract T instanceFromRow(DbRow row);

    protected abstract HashMap<String, Object> lineFromInstance(T element);

    protected String getElementString(T elt){
        return elt.toString();
    }

    public enum Order {
        ASC, DESC;

        public static Order fromSortOrderString(String sortOrderString) {
            if(TextUtils.isEmpty(sortOrderString)) return null;
            if(sortOrderString.contains(ASC.name())) return ASC;
            if(sortOrderString.contains(DESC.name())) return DESC;
            return null;
        }
    }
}
