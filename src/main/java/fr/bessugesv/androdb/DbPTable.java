package fr.bessugesv.androdb;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.bessugesv.androdb.fields.AbsDbField;
import fr.bessugesv.androdb.fields.DbRow;

/**
 * Created by Vincent on 18/11/2014.
 */
public abstract class DbPTable<T> extends DbTable<DbPTableSchema<T>> {
    protected DbPTableSchema.Order mOrder = null;
    protected String mOrderingColumn;

    public DbPTable(Database db, DbPTableSchema<T> schema){
        super(db, schema);
    }

    public synchronized ArrayList<T> loadAll() {
        return loadAllOrdered(mOrder, mOrderingColumn);
    }

    public synchronized ArrayList<T> loadAllOrdered(DbPTableSchema.Order order, String orderingColumn) {
        return loadAllOrderedAndSelected(order, orderingColumn, null, null);
    }

    public synchronized ArrayList<T> loadAllSelected(String selection, String[] selectionArgs) {
        return loadAllOrderedAndSelected(mOrder, mOrderingColumn, selection, selectionArgs);
    }

    public synchronized ArrayList<T> loadAllOrderedAndSelected(DbPTableSchema.Order order, String orderingColumn, String selection, String[] selectionArgs) {
        ArrayList<T> result = new ArrayList<T>();
        Cursor cursor = query(order, orderingColumn, selection, selectionArgs);

        if (cursor == null || cursor.getCount() == 0) {
            return result;
        }
        cursor.moveToFirst();
        do {
            DbRow row = new DbRow();
            for (AbsDbField field : mSchema.getFields()) {
                field.fillRow(cursor, row);
            }
            result.add(mSchema.instanceFromRow(row));

        } while (cursor.moveToNext());

        cursor.close();

        return result;
    }

    public synchronized void save(T element) {
        save(mDb.getWritableDatabase(), element);
    }

    public synchronized long save(SQLiteDatabase wdb,  T element) {
        long id = -1;
        try {
            mDb.lock();
            mDb.beginTransaction(wdb);
            SQLiteStatement stmt = getSaveStatement(wdb);
            id = doSave(element, stmt);
            mDb.commit(wdb);
        } catch (Throwable t) {
            mDb.log(Log.ERROR, formatLog("Error while saving element " + element), t);
            mDb.rollback(wdb);
        } finally {
            mDb.unlock();
            return id;
        }
    }

    public synchronized void save(List<T> elements){
        save(mDb.getWritableDatabase(), elements);
    }

    public synchronized void save(SQLiteDatabase wdb, List<T> elements) {
        T lastElement = null;
        try {
            mDb.lock();
            mDb.beginTransaction(wdb);
            SQLiteStatement stmt = getSaveStatement(wdb);
            for (T element : elements) {
                lastElement = element;
                doSave(element, stmt);
            }
            mDb.commit(wdb);
        } catch (Throwable t) {
            mDb.log(Log.ERROR, formatLog("Error while saving element " + mSchema.getElementString(lastElement)), t);
            mDb.rollback(wdb);
        } finally {
            mDb.unlock();
        }
    }

    private SQLiteStatement getSaveStatement(){
        return getSaveStatement(mDb.getReadableDatabase());
    }

    private SQLiteStatement getSaveStatement(SQLiteDatabase db) {
        String key = "save";
        SQLiteStatement stmt = mDb.getKeptStatement(this, key);
        if (stmt == null) {
            ArrayList<AbsDbField> fields = mSchema.getFields();
            if (fields.size() == 0) return null;
            StringBuilder values = new StringBuilder(" VALUES (?");
            StringBuilder sql = new StringBuilder("INSERT OR REPLACE INTO ").append(mSchema.getTableName());
            sql.append("(" + fields.get(0).getName());
            for (int i = 1; i < fields.size(); i++) {
                sql.append(", " + fields.get(i).getName());
                values.append(", ?");
            }
            sql.append(")" + values + ")");
            values.append(")");
            Log.d(LOG, "save statement: " + sql.toString());
            stmt = mDb.prepareStatement(db, sql.toString());
            mDb.keepStatement(this, key, stmt);
        }
        return stmt;
    }

    private long doSave(T element, SQLiteStatement stmt) {
        HashMap<String, Object> line = mSchema.lineFromInstance(element);
        ArrayList<AbsDbField> fields = mSchema.getFields();
        for (int i = 0; i < fields.size(); i++) {
            AbsDbField field = fields.get(i);
            field.bind(stmt, line.get(field.getName()), i + 1);
        }
        return stmt.executeInsert();
    }


    /**
     * Delete elements from the table.<br>
     * Will use a set of columns as criterias, and an element to match with.
     *
     * @param element the element used as match in the query
     * @param columns the criterias of the query - MUST NOT BE EMPTY
     * @return if there was no errors
     */
    public synchronized boolean delete(T element, String[] columns) {
        try {
            mDb.lock();

            StringBuilder whereClause = new StringBuilder("WHERE " + columns[0] + "=?");
            for (int i = 1; i < columns.length; i++)
                whereClause.append(" AND ").append(columns[i]).append("=?");

            HashMap<String, Object> line = mSchema.lineFromInstance(element);
            String [] whereArgs = new String[columns.length];
            for (int i = 0 ; i < columns.length ; i++) {
                whereArgs[i] = String.valueOf(line.get(columns[i]));
            }

            mDb.getWritableDatabase().delete(mSchema.getTableName(), whereClause.toString(), whereArgs);
        } catch (Throwable t) {
            mDb.log(Log.ERROR, formatLog("Could not delete element from table "+mSchema.getElementString(element)));
            return false;
        } finally {
            mDb.unlock();
        }
        return true;

    }
}
