package fr.bessugesv.androdb;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;

/**
 * Created by Vincent on 23/11/2014.
 */
public class ADContentUris extends ContentUris {
    private static final String TYPE_PART_SINGULAR  = "vnd.android.cursor.item";
    private static final String TYPE_PART_PLURAL    = "vnd.android.cursor.dir";

    public static final int SINGULAR    = 1;
    public static final int PLURAL      = 2;

    public static Uri buildContentUri(String path){
        return Uri.parse(String.format("%s://%s", ContentResolver.SCHEME_CONTENT, path));
    }

    public static Uri buildContentUri(String authority, String path){
        return Uri.parse(String.format("%s://%s/%s", ContentResolver.SCHEME_CONTENT, authority, path));
    }

    public static String buildType(int typeNumber, String authority, String name){
        return String.format("%s/vnd.%s.%s", formatTypePart(typeNumber), authority, name);
    }

    public static String formatTypePart(int typeNumber){
        switch(typeNumber){
            case SINGULAR:  return TYPE_PART_SINGULAR;
            case PLURAL:    return TYPE_PART_PLURAL;
        }
        return null;
    }

    public static int getTypeNumber(String type){
        if(type.startsWith(TYPE_PART_SINGULAR)){
            return SINGULAR;
        }
        if(type.startsWith(TYPE_PART_PLURAL)){
            return PLURAL;
        }
        return -1;
    }
}
