package fr.bessugesv.androdb;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import java.util.Date;

/**
 * Created by Vincent Bessuges on 30/11/2014.
 */
public class CursorHelper {

    public static String getString(Cursor cursor, String columnName){
        int index = checkColumnIndex(cursor, columnName);
        return cursor.getString(index);
    }

    public static int getInt(Cursor cursor, String columnName){
        int index = checkColumnIndex(cursor, columnName);
        return cursor.getInt(index);
    }

    public static long getLong(Cursor cursor, String columnName){
        int index = checkColumnIndex(cursor, columnName);
        return cursor.getLong(index);
    }

    public static Date getDate(Cursor cursor, String columnName) {
        int index = checkColumnIndex(cursor, columnName);
        return new Date(cursor.getLong(index) * 1000L);
    }

    public static boolean getBoolean(Cursor cursor, String columnName) {
        int index = checkColumnIndex(cursor, columnName);
        return cursor.getInt(index) != 0;
    }

    private static int checkColumnIndex(Cursor cursor, String columnName){
        int index = cursor.getColumnIndex(columnName);
        if(index < 0) Log.e("Cursor", String.format("ERROR: no column [%s] found in cursor - cols: %s", columnName, TextUtils.join(", ", cursor.getColumnNames())));
        return index;
    }
}
