package fr.bessugesv.androdb;

import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.HashMap;

import fr.bessugesv.androdb.fields.AbsDbField;
import fr.bessugesv.androdb.fields.DbIdField;

/**
 * Created by VBessuges on 10/04/2015.
 */
public abstract class DbTableSchema {
    protected String mTableName;
    protected ArrayList<AbsDbField> mFields;
    protected HashMap<String, AbsDbField> mFieldMap;
    protected ArrayList<DbIndex> mIndexes;
    protected ArrayList<DbJoin> mJoins;
    protected boolean mHasID = false;

    public String getTableName(){
        return mTableName;
    }

    public ArrayList<AbsDbField> getFields(){
        return mFields;
    }

    public ArrayList<DbIndex> getIndexes(){
        return mIndexes;
    }

    public ArrayList<DbJoin> getJoins() {
        return mJoins;
    }

    public HashMap<String, AbsDbField> getFieldMap(){
        return mFieldMap;
    }

    public DbTableSchema(){

        mTableName = _getTableName();

        mFields = _getFields();

        if(this instanceof BaseColumns){
            mHasID = true;
            mFields.add(0, new DbIdField());
        }

        mFieldMap = new HashMap<String, AbsDbField>();
        for (AbsDbField field : mFields) {
            mFieldMap.put(field.getName(), field);
        }

        mIndexes = _getIndexes();

        mJoins = _getJoins();
    }

    public abstract String _getTableName();

    protected abstract ArrayList<AbsDbField> _getFields();

    protected abstract ArrayList<DbIndex> _getIndexes();

    protected abstract ArrayList<DbJoin> _getJoins();
}
