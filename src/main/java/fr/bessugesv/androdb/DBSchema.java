package fr.bessugesv.androdb;

import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by Vincent Bessuges on 19/11/2014.
 */
@SuppressWarnings("rawtypes")
public abstract class DBSchema {
    protected ArrayList<DbTable> mTables;

    public void setDatabase(Database db){
        mTables = initTables(db);
        for(DbTable table : mTables){
            table.init();
        }
    }

    public ArrayList<DbTable> getTables(){
        return mTables;
    }

    public DbTable getTable(String tableName) {
        for(DbTable table : mTables){
            if(TextUtils.equals(table.getSchema().getTableName(), tableName)) return table;
        }
        return null;
    }

    public void onCreate(SQLiteDatabase db) {
        for(DbTable dbTable : mTables){
            dbTable.createTable(db);
        }
    }

    public void onUpgrade(SQLiteDatabase db, int to){
        for(DbTable dbTable : mTables){
            dbTable.upgradeFromPrevious(db, to);
        }
    }


    public abstract String getDBName();
    public abstract int getDBVersion();
    public abstract ArrayList<DbTable> initTables(Database db);

}
