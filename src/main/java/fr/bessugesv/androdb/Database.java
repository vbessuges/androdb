package fr.bessugesv.androdb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Vincent on 17/11/2014.
 */
public abstract class Database extends SQLiteOpenHelper {
    private static final String TAG = Database.class.getSimpleName();

    private ReentrantLock mLock = new ReentrantLock();
    public void lock(){
        mLock.lock();
    }
    public void unlock(){
        mLock.unlock();
    }

    protected Context mContext;
    private DBSchema mContract;
    private HashMap<String, SQLiteStatement> mStatements = new HashMap<String, SQLiteStatement>();

    public Context getContext(){
        return mContext;
    }

    public DBSchema getContract(){
        return mContract;
    }

    public Database(Context context, DBSchema contract){
        super(context, contract.getDBName(), null, contract.getDBVersion());
        mContext = context;
        mContract = contract;
        mContract.setDatabase(this);
    }


    public SQLiteDatabase beginTransaction() {
        return beginTransaction(getWritableDatabase());
    }


    public SQLiteDatabase beginTransaction(SQLiteDatabase wdb) {
        wdb.beginTransaction();
        return wdb;
    }

    public void commit(SQLiteDatabase db){
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void rollback(SQLiteDatabase db){
        if(db == null) return;
        db.endTransaction();
    }



    private String buildStatementKey(DbTable table, String key){
        return table.getSchema().getTableName() + "-" + key;
    }

    public void keepStatement(DbTable table, String key, SQLiteStatement stmt) {
        mStatements.put(buildStatementKey(table, key), stmt);
    }

    public SQLiteStatement getKeptStatement(DbTable table, String key) {
        SQLiteStatement stmt = mStatements.get(buildStatementKey(table, key));
        if(stmt != null) stmt.clearBindings();
        return stmt;
    }

    public SQLiteStatement prepareStatement(String sql){
        return prepareStatement(getReadableDatabase(), sql);
    }

    public SQLiteStatement prepareStatement(SQLiteDatabase rdb, String sql){
        SQLiteStatement stmt = rdb.compileStatement(sql);
        stmt.clearBindings();
        return stmt;
    }




    @Override
    public void onCreate(SQLiteDatabase db) {
        mContract.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for(int i = oldVersion ; i <= newVersion ; i++){
            mContract.onUpgrade(db, i);
        }
    }


    public void log(int level, String msg) {
        Log.println(level, TAG, msg);
    }

    public void log(int level, String msg, Throwable t) {
        switch(level){
            case Log.DEBUG:     Log.d(TAG, msg, t); break;
            case Log.ERROR:     Log.e(TAG, msg, t); break;
            case Log.INFO:      Log.i(TAG, msg, t); break;
            case Log.VERBOSE:   Log.v(TAG, msg, t); break;
            case Log.WARN:      Log.w(TAG, msg, t); break;
        }
    }


}
