package fr.bessugesv.androdb;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;

import fr.bessugesv.androdb.fields.AbsDbField;

/**
 * Created by Vincent on 22/11/2014.
 */
public abstract class DbTable<DBTS extends DbTableSchema> {
    public static final String LOG = DbPTable.class.getSimpleName();

    protected Database mDb;
    protected DBTS mSchema;


    public Context getContext(){
        return mDb.getContext();
    }

    public DBTS getSchema(){
        return mSchema;
    }

    public DbTable(Database database, DBTS schema) {
        mDb = database;
        mSchema = schema;
    }

    protected void init(){}

    public synchronized void createTable(SQLiteDatabase db) {
        try {
            String tableName = mSchema.getTableName();
            ArrayList<AbsDbField> fields = mSchema.getFields();
            ArrayList<DbIndex> indexes = mSchema.getIndexes();

            // making sure no one touches the db
            mDb.lock();

            Log.i(LOG, "Table Creation " + tableName);

            if (recreateTable() && existsInDb(db)) {
                SQLiteStatement stmt = db.compileStatement("DROP TABLE " + tableName);
                stmt.execute();
                stmt.close();
            }
            if (fields.size() == 0 || existsInDb(db)) {
                return;
            }

            // Table creation fields
            StringBuilder sql = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append(tableName);
            sql.append(" (" + fields.get(0).toDBCreateString());
            for (int i = 1; i < fields.size(); i++) {
                sql.append(", " + fields.get(i).toDBCreateString());
            }
            sql.append(")");
            Log.d(LOG, "preparing sql statement: " + sql.toString());
            SQLiteStatement stmt = db.compileStatement(sql.toString());
            stmt.execute();
            stmt.close();

            // Adding indexes
            if (indexes == null || indexes.size() == 0) {
                return;
            }
            for (DbIndex index : indexes) {
                String sStmt = index.toDBCreateString(tableName);
                Log.d(LOG, "preparing sql statement: " + sStmt);
                stmt = db.compileStatement(sStmt);
                stmt.execute();
                stmt.close();
            }

        } finally {
            mDb.unlock();
        }
    }

    public synchronized void checkTableIntegrity(SQLiteDatabase db) {
        Log.w(LOG, "===TABLE INTEGRITY CHECK START (" + mSchema.getTableName() + ")");
        for (AbsDbField field : retrieveNonExistingFields(db)) {
            Log.e(LOG, "One field is missing: " + field.getName());
            addField(db, field);
        }
        Log.w(LOG, "===TABLE INTEGRITY CHECK END (" + mSchema.getTableName() + ")");

    }

    public boolean existsInDb(SQLiteDatabase db) {
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + mSchema.getTableName() + "'", null);
            if (cursor != null && cursor.getCount() > 0) {
                return true;
            }
        } catch (Throwable t) {
            Log.e(LOG, t.getMessage(), t);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return false;
    }

    @SuppressWarnings("rawtypes")
    public ArrayList<AbsDbField> retrieveNonExistingFields(SQLiteDatabase db) {
        ArrayList<AbsDbField> notExisting = new ArrayList<AbsDbField>();
        try {
            Cursor res = db.rawQuery("SELECT * FROM " + mSchema.getTableName() + " LIMIT 0,1 ", null);
            for (AbsDbField f : mSchema.getFields()) {
                if (res.getColumnIndex(f.getName()) == -1)
                    notExisting.add(f);
            }
            res.close();
        } catch (SQLException e) {
            Log.w(LOG, "Error accessing column " + e.toString());
        }
        return notExisting;
    }

    @SuppressWarnings("rawtypes")
    public boolean fieldExistsInTable(SQLiteDatabase db, AbsDbField field) {
        try {
            Cursor res = db.rawQuery("SELECT * FROM " + mSchema.getTableName() + " LIMIT 0,1 ", null);
            boolean exists = res.getColumnIndex(field.getName()) >= 0;
            res.close();
            return exists;
        } catch (SQLException e) {
            Log.w(LOG, "Error accessing column " + e.toString());
        }
        return false;
    }

    public synchronized void addField(SQLiteDatabase db, AbsDbField field) {
        Log.i(LOG, "addField " + field.getName());
        try {

            mDb.lock();
            String tableName = mSchema.getTableName();

            StringBuilder sql = new StringBuilder("ALTER TABLE ").append(tableName).append(" ADD COLUMN " + field.toDBCreateString());
            Log.d(LOG, "preparing sql statement: " + sql.toString());
            SQLiteStatement stmt = db.compileStatement(sql.toString());
            stmt.execute();
            stmt.close();
        } catch (SQLiteException sqle) {
            if (fieldExistsInTable(db, field)) {
                Log.e(LOG, "Column already existed!");
            } else throw sqle;
        } finally {
            mDb.unlock();
        }

    }

    public synchronized Cursor query(DbPTableSchema.Order order, String orderingColumn, String selection, String[] selectionArgs){
        String sortOrder = null;
        if(order != null && !TextUtils.isEmpty(orderingColumn)){
            sortOrder = orderingColumn + " " + order;
        }
        return query(selection, selectionArgs, sortOrder);
    }

    public synchronized Cursor query(String selection, String[] selectionArgs, String sortOrder) {
        Cursor result = null;
        String tableName = mSchema.getTableName();
        ArrayList<AbsDbField> fields = mSchema.getFields();
        ArrayList<DbJoin> joins = mSchema.getJoins();
        boolean hasJoins = joins != null && !joins.isEmpty();
        int totalColumn = fields.size();
        if(hasJoins){
            for(DbJoin join : joins) totalColumn += join.getSelection().length;
        }
        try {
            mDb.lock();

            String[] colums = new String[totalColumn];
            for (int i = 0; i < fields.size(); i++) {
                colums[i] = (hasJoins ? tableName + "." : "") + fields.get(i).getName();
            }
            if(hasJoins){
                int index = fields.size();
                for(DbJoin join : joins){
                    for(String col : join.getSelection()){
                        colums[index++] = join.getDestinationTableName() + "." + col;
                    }
                }
            }
            // SELECT
            StringBuilder sbQuery = new StringBuilder("SELECT ");
            // columns
            sbQuery.append(TextUtils.join(", ", colums));
            // FROM table
            sbQuery.append(" FROM ").append(tableName);
            // join clause
            if(hasJoins){
                for(DbJoin join : joins){
                    String destTableName = join.getDestinationTableName();
                    sbQuery.append(" INNER JOIN ").append(destTableName)
                            .append(" ON ")
                            .append(tableName).append(".").append(join.getKeyOrigin())
                            .append(" = ")
                            .append(destTableName).append(".").append(join.getKeyDestination());
                }
            }
            // WHERE
            if(!TextUtils.isEmpty(selection)){
                sbQuery.append(" WHERE ").append(selection);
            }
            // ORDER BY
            if(!TextUtils.isEmpty(sortOrder)){
                sbQuery.append(" ORDER BY ").append(sortOrder);
            }

            String query = sbQuery.toString();
            mDb.log(Log.INFO, "Query:" + query);

            result = mDb.getReadableDatabase().rawQuery(query, selectionArgs);

        } catch (Throwable t) {
            mDb.log(Log.ERROR, "Could not load the elements from the table " + tableName + ".", t);
            return null;
        } finally {
            mDb.unlock();
        }
        return result;
    }

    public synchronized boolean deleteByID(String id) {
        SQLiteDatabase wdb = null;
        try {
            mDb.lock();
            wdb = mDb.beginTransaction();
            String key = "deleteById";
            SQLiteStatement stmt = mDb.getKeptStatement(this, key);
            if (stmt == null) {
                stmt = mDb.prepareStatement("DELETE FROM " + mSchema.getTableName() + " WHERE "+ BaseColumns._ID+" = ?");
                mDb.keepStatement(this, key, stmt);
            }
            stmt.bindString(1, id);
            stmt.executeInsert();
            mDb.commit(wdb);
            return true;
        } catch (Throwable t) {
            mDb.rollback(wdb);
            return false;
        } finally {
            mDb.unlock();
        }
    }

    public synchronized boolean selectionHasResults(String[] columns, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        String tableName = mSchema.getTableName();
        try {
            mDb.lock();
            cursor = mDb.getReadableDatabase().query(tableName, columns, selection, selectionArgs, null, null, null);
            return cursor.getCount() > 0;
        } catch (Throwable t) {
            mDb.log(Log.ERROR, formatLog("Error trying to know if selection [" + selection + "] had results"), t);
        } finally {
            mDb.unlock();
            if (cursor != null)
                cursor.close();
        }
        return false;
    }

    public synchronized int countAsResult(String selection, String[] selectionArgs) {
        Cursor cursor = null;
        String tableName = mSchema.getTableName();
        try {
            mDb.lock();
            cursor = mDb.getReadableDatabase().rawQuery("select count(*) from " + tableName + (selection == null ? "" : selection), selectionArgs);
            return cursor.getCount();
        } catch (Throwable t) {
            mDb.log(Log.ERROR, formatLog("\"Error trying to know count of selection [\" + selection + \"]"), t);
        } finally {
            mDb.unlock();
            if (cursor != null)
                cursor.close();
        }
        return 0;
    }

    // DEBUG PURPOSES: can be overridden by the children
    private boolean recreateTable() {
        return false;
    }

    protected String formatLog(String msg){
        return String.format("[%s] %s", LOG + "-" + mSchema.getTableName(), msg);
    }

    public abstract void upgradeFromPrevious(SQLiteDatabase db, int to);
}
