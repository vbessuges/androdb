package fr.bessugesv.androdb;

/**
 * Created by Vincent Bessuges on 14/05/2015.
 */
public class DbJoin {
    /**
     * Column in the origin table that will be used in the join-predicate.
     */
    private String mKeyOrigin;
    /**
     * Joined table name.
     */
    private String mDestinationTableName;
    /**
     * Column in the joined table that will be used in the join-predicate.
     */
    private String mKeyDestination;
    /**
     * Selection of columns that will be retrieved from the joined table.
     */
    private String [] mSelection;

    public String getKeyOrigin() {
        return mKeyOrigin;
    }

    public String getDestinationTableName(){
        return mDestinationTableName;
    }

    public String getKeyDestination(){
        return mKeyDestination;
    }

    public String [] getSelection(){
        return mSelection;
    }


    public DbJoin(String keyOrigin, String destinationTableName, String keyDestination, String [] selection){
        mKeyOrigin = keyOrigin;
        mDestinationTableName = destinationTableName;
        mKeyDestination = keyDestination;
        mSelection = selection;
    }




}
