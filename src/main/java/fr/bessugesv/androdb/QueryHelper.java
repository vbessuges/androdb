package fr.bessugesv.androdb;

/**
 * Created by Vincent on 7/31/2016.
 */
public class QueryHelper {

    public static QuerySelection buildSelectionForInQuery(String columnName, long [] args) {
        Long[] oArgs = new Long[args.length];
        for (int i = 0 ; i < args.length ; i++) oArgs[i] = args[i];
        return buildSelectionForInQuery(columnName, oArgs);
    }

    public static <T> QuerySelection buildSelectionForInQuery(String columnName, T [] args) {
        int length = args.length;
        if (length < 1) {
            throw new IllegalArgumentException("Can't build placeholder for empty args.");
        }
        QuerySelection querySelection = new QuerySelection();
        querySelection.selectionArgs = new String[length];

        // building the auery selection
        StringBuilder sb = new StringBuilder(columnName + " IN (?");
        querySelection.selectionArgs[0] = String.valueOf(args[0]);
        for (int i = 1 ; i < length ; i++) {
            sb.append(",?");
            querySelection.selectionArgs[i] = String.valueOf(args[i]);
        }
        sb.append(")");
        querySelection.selection = sb.toString();

        return querySelection;
    }

    public static class QuerySelection {

        public String selection;
        public String[] selectionArgs;
    }
}
